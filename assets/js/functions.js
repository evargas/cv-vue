// attibutes & methods
	let content = {
		page:1, 
		prev:0,
		next:2,
		page_content:[
			{
				title:'',
				content:''
			},
			{
				title: 'Mis Habilidades',
				content:''
			},
			{
				title: 'Un Poco Sobre Mi',
				content:'Apasionado por la ciencia y tecnología, despues de estudiar química en la Universidad Central de Venezuela, incursioné en el mundo de la programación en busqueda de una rama de la química llamada "Química Computacional", mis primeros  pasos en la programacion fué con MatLab y FORTRAN enamorandome cada día mas de la simulación de procesos. En PDVSA me dieron la oportunidad de ir maquetando algunas vistas con bootstrap para su intranet y desde alli supe lo que queria hacer el resto de mi vida.'
			},
			{
				title: 'Experiencia Laboral',
				content: '', 
				collapse: [
					false, 
					false, 
					false,
					false
				],
			},
			{
				title: 'Cursos Realizados',
				content: ''
			},
			{
				title: 'Referencias',
				content: ''
			},
			{
				title: 'Projecto CV',
				content: 'Este es un pequeño demo de algunas de mis habilidades, tiene un poco de Vue2js, html5, css3, less, bootstrap y jquery. Puedes ver el projecto completo en gitlab.',
				link: 'https://gitlab.com/evargas/cv-vue'
			}
		],
	};

	let cv = {
		person : {
			name : 'Erick',
			lastname : 'Vargas', 
			ci: '13615520',
			date: '02/01/1977',
			status: 'casado',
			children: 3,
			nacionality: 'venezuelano',
			phone: '+5804169049644',
			image:'assets/img/erick.jpg',
			email:'erickorso@gmail.com',
			linkedin: 'https://www.linkedin.com/in/vargas-erick-1ab86b102/', 
			address: {
				country:'Venezuela', 
				state: 'Miranda',
				city: 'San Antonio de los Altos',
				localidad: 'El Amarillo',
				calle: 'El Guamal Norte 2c',
				casa: 'Los Peña',  
				number: '05',
				zipcode: '1204'
			}, 
		}, 
		studies: {
			university : [
				{
					name: 'Universidad Central de Venezuela',
					alias: 'UCV',
					title: 'Licenciado en Química',
					start: '1996',
					end : '2002',
					complete: true
				},
				{
					name: 'Universidad Nacional Experimental de las Fuerzas Armadas',
					alias: 'unefa',
					title: 'ingeniero de sistemas',
					start: '2005',
					end : '2010',
					complete: false
				}
			]
		},
		curses:[
			{
				name: 'desarrollo de framework php',
				institution: 'ucv',
				hours: 32,
				area: 'web',
				year: 2015,
			},
			{
				name: 'themes para wordpress',
				institution: 'ucv',
				hours: 40,
				area: 'web',
				year: 2015,
			},
			{
				name: 'evaluaciones económicas',
				institution: 'pdvsa',
				hours: 16,
				area: 'finanzas',
				year: 2013,
			},
			{
				name: 'microscopía electrónica avanzada',
				institution: 'química',
				hours: 20,
				area: 'ivic',
				year: 2012,
			},
			{
				name: 'programación avanzada con php',
				institution: 'ucv',
				hours: 60,
				area: 'web',
				year: 2012,
			},
			{
				name: 'programación con php',
				institution: 'ucv',
				hours: 20,
				area: 'web',
				year: 2012,
			},
			{
				name: 'contraloría social y ciudadana',
				institution: 'pdvsa',
				hours: 4,
				area: 'finanzas',
				year: 2012,
			},
			{
				name: 'auditoría interna iso 9001',
				institution: 'pdvsa',
				hours: 32,
				area: 'finanzas',
				year: 2012,
			},
			{
				name: 'preveción de accidentes viales',
				institution: 'pdvsa',
				hours: 4,
				area: 'siho',
				year: 2012,
			},
			{
				name: 'manejo de emergencias 2',
				institution: 'pdvsa',
				hours: 16,
				area: 'siho',
				year: 2012,
			},
			{
				name: 'manejo de extintores',
				institution: 'pdvsa',
				hours: 8,
				area: 'siho',
				year: 2011,
			},
			{
				name: 'primeros auxilios',
				institution: 'pdvsa',
				hours: 8,
				area: 'siho',
				year: 2011,
			},
			{
				name: 'manejo de emergencias 1',
				institution: 'pdvsa',
				hours: 8,
				area: 'siho',
				year: 2011,
			},
			{
				name: 'desalojo de edificaciones',
				institution: 'cesg',
				hours: 12,
				area: 'siho',
				year: 2011,
			},
		],
		jobs: [
			{
				company: 'kavak.com', 
				country: 'mexico',
				months: 12,
				position: 'sr web developer',
				last_year: '2016-2017',
				description: 'Desarrollo y optimización del portal web, desarrollo de aplicaciones propias de la lógica de negocio', 
				tecnologies: 'php, codeigniter, html5, css3, bootstrap, less, git, javascript, jquery,  vue2, api rest,  mysql'
			},
			{
				company: 'screenmediagroup.com', 
				country: 'venezuela',
				months: 20,
				position: 'sr web developer',
				last_year: '2014-2016',
				description: 'Desarrollo de temas y plugins para wordpress, codeigniter y cakephp, maquetacion de webwalls y landing pages, webmails, etc',
				tecnologies: 'php, wordpress, cakephp, codeigniter, html5, css3, bootstrap, less, git, javascript, jquery,  mysql'
			},
			{
				company: 'freelance', 
				country: 'international',
				months: 60,
				position: 'sr web developer',
				last_year: '2012-2016',
				description: 'Experto en maquetación con bootstrap o flexbox, responsive web design,  desarrollo de temas y plugins para wordpress, codeigniter y cakephp, maquetación de webwalls y landing pages, webmails.',
				tecnologies: 'php, wordpress, cakephp, codeigniter, html5, css3, bootstrap, less, git, javascript, jquery,  angular,  api rest,  mysql'
			},
			{
				company: 'pdvsa', 
				country: 'venezuela',
				months: 38,
				position: 'analista de finanzas',
				last_year: '2010-2013',
				description: 'Monitorear diversos sistemas financieros, administracion del workflow (SAP,  MPT, SIRET, GADET, RESET), asesoramiento en calidad, normas y procedimientos, adiestrar, supervisar y auditar personal de la gerencia de finanzas. Tambien tuve tres cargos anteriores, operaciones bancarias (2012), analista de cuentas por pagar (2011) y pasante de finanzas (2010)',
				tecnologies: 'php, wordpress, cakephp, codeigniter, html5, css3, bootstrap, less, git, javascript, jquery,  angular,  api rest'
			},
		],
		knowlege:[
			{
				name:'php', 
				level:'sr',
			},
			{
				name:'codeigniter', 
				level:'sr',
			},
			{
				name:'wordpress', 
				level:'sr',
			},
			{
				name:'html5', 
				level:'sr',
			},
			{
				name:'css3', 
				level:'sr',
			},
			{
				name:'jquery', 
				level:'sr',
			},
			{
				name:'ajax', 
				level:'sr',
			},
			{
				name:'bootstrap', 
				level:'sr',
			},
			{
				name:'less', 
				level:'sr',
			},
			{
				name:'flexbox', 
				level:'sr',
			},
			{
				name:'responsive web design', 
				level:'sr',
			},
			{
				name:'javascript', 
				level:'ssr',
			},
			{
				name:'git', 
				level:'ssr',
			},
			{
				name:'mysql', 
				level:'ssr',
			},
			{
				name:'vue2', 
				level:'ssr',
			},
			{
				name:'ux/ui', 
				level:'ssr',
			},
			{
				name:'react', 
				level:'jr',
			},
			{
				name:'angular', 
				level:'jr',
			},
			{
				name:'seo', 
				level:'jr',
			},

		],
		references: [
			{
				name:'Alejandro Gouveia', 
				company: 'kavak.com',
				position:'co-director',
				email: 'alex@kavak.com',
				phone: '+584129091179',
			},
			{
				name:'Antony Delgado', 
				company: 'kavak.com',
				position:'sr developer',
				email: 'antony.delgado@kavak.com',
				phone: '+5215554719453',
			},
			{
				name:'Idelfonso Sanchez', 
				company: 'kavak.com',
				position:'sr developer',
				email: 'idelfonso@kavak.com',
				phone: '+584122860241',
			},
			{
				name:'Ruben Camargo', 
				company: 'hdbuzios.com',
				position:'sr developer',
				email: 'rubencamargo@gmail.com',
				phone:'+541127037330'
			},
		],
	};

	//vue object

	function vue_init(){
		var app = new Vue({
			el: '#vue-app',
			data: {
				cv     : cv,
				content: content,
			},
			methods: {
				show_page: function(index){
					index = parseInt(index);
					this.content.page = index;
					this.content.prev = index - 1;
					this.content.next = index + 1;
				}, 
				jobs_collapse: function(index){
					this.content.page_content[3].collapse[index] != this.content.page_content[3].collapse[index];
				}
			}, 
			computed:{
				full_name: function(){
					var full_name = this.cv.person.name + ' ' + this.cv.person.lastname;
					return full_name;
				}, 
				last_position: function(){
					var position = this.cv.jobs[1].position;
					return position.toUpperCase();
				}, 
				email_link: function(){
					return 'mailto:' + this.cv.person.email;
				}, 
				knowlege: function(){
					return this.cv.knowlege;
				}, 
				jobs: function(){
					return this.cv.jobs;
				},
				curses: function(){
					return this.cv.curses;
				}, 
				references: function(){
					return this.cv.references;
				}
			}
		})
	}

//document ready
$(function(){

//init

	//vue object
	vue_init();

	// preloader animation
	$( "#preloader" ).animate({
		opacity      : 0,
		left         : "+=400",
		right        : "+=400",
		top          : "+=500",
		bottom       : "+=500", 
		borderRadius : "+=500" 
	}, 2000, function() {
		//hide preloader
		$(this).fadeOut(500);
	});

	$( "#preloader-bellow1" ).animate({
		opacity      : 0,
		left         : "-=700",
		right        : "-=700",
		top          : "-=500",
		bottom       : "-=500", 
		borderRadius : "-=1" 
	}, 2200, function() {
		//hide preloader
		$(this).fadeOut(500);
	});

	$( "#preloader-bellow2" ).animate({
		opacity      : 0,
		left         : "-=200",
		right        : "-=200",
		top          : "-=200",
		bottom       : "-=200", 
	}, 2400, function() {
		//hide preloader
		$(this).fadeOut(500);
	});

	$( "#preloader-bellow3" ).animate({
		opacity      : 0,
		left         : "-=300",
		right        : "-=300",
		top          : "-=300",
		bottom       : "-=300", 
	}, 2600, function() {
		//hide preloader
		$(this).fadeOut(500);
	});

	$('.list-group-item').click(function(){
		if ($(this).find('.fa').hasClass('fa-plus')) {
			$(this).find('.fa').addClass('fa-minus').removeClass('fa-plus');
		}else{
			$(this).find('.fa').addClass('fa-plus').removeClass('fa-minus');
		}
	})
})